console.log("Hello World");

console . log (" Hello World ");

console.
log
(
"Hello Again"
)

let myVariable;
let hello;

console.log(myVariable);
console.log(hello);

let firstName = "Hillary";


let pokemon = 25000;

let productName = "desktop computer";
console.log(productName)

let productPrice = 18999;
console.log(productPrice)

let friend = "Kate";


let province = "Metro Manila";
let country = "Philippines";

let greeting = "I live in the " + country;
console.log(greeting);

// Escape Character (\)
// "\n" refers to creating a new line between text

let message = "John's employees went home early";
console.log(message);

message = 'John\'s employees went home early';
console.log(message)

// Numbers

let headcount = 26;
console.log(headcount);

// Decimal or Functions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
console.log("John's grade last quarter is " + grade);

// Boolean
// We have 2 boolean values, true and false

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct" + isGoodConduct);

// Arrays are special kind of data type
// It is used to store multiple values with the same data type
// Syntax -> let/const arrayName = [elementA, elementB, ... , elementZ]

let grades = [98.7, 92.1, 90.2];
console.log(grades);

// It is not a best practice to combine different data types
let details = ["John", "Smith", 32, true]
console.log(details);

// Objects
// Hold properties that describes the variable
// Syntax -> let/const objectName = {propertyA: value, propertyB: value}

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	contact: ["0123456789", "9876543210"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
}

console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}

console.log(myGrades);

// Checking the data type
console.log(typeof grades);
console.log(typeof person);
console.log(typeof isMarried);

// In programming, we alsways start counting from 0
const anime = ["One Piece", "One Punch Man", "Attack on Titan"];
//anime = ["Kimetsu No Yaiba"];
anime[0] = "Kimetsu No Yaiba";

console.log(anime)

//Null vs Undefined
// It is Null when a variable has 0 or empty value
// It is Undefeined when a variable has no value upon declaration
let spouse = 0; // null == 0 == empty

let fullName; // undefined